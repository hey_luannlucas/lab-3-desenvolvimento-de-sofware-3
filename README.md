# Music Library API

Este é um projeto de exemplo para uma API de biblioteca de músicas, desenvolvido com Spring Boot e utilizando o Spring Data JPA para interagir com o banco de dados.

## Estrutura do Projeto

O projeto é estruturado da seguinte forma:

- **`com.api.jala.university.musiclibrary.demo.controller`**: Este pacote contém os controladores REST da API, onde estão definidos os endpoints para adicionar, excluir, atualizar e pesquisar músicas.

- **`com.api.jala.university.musiclibrary.demo.entity`**: Aqui estão as entidades JPA que representam os modelos de dados da aplicação. No caso, temos a entidade `Music`, que representa uma música com atributos como título, artista, gênero e duração.

- **`com.api.jala.university.musiclibrary.demo.infra.swagger`**: Contém a configuração do OpenAPI (anteriormente conhecido como Swagger) para documentação da API.

- **`com.api.jala.university.musiclibrary.demo.repository`**: Repositórios JPA que definem métodos de consulta para interagir com o banco de dados.

- **`com.api.jala.university.musiclibrary.demo.service`**: Serviços que encapsulam a lógica de negócios da aplicação, responsáveis por adicionar, excluir, atualizar e pesquisar músicas.

- **`application.properties`**: Arquivo de configuração do Spring Boot onde são definidas as configurações do banco de dados. Atualmente, está configurado para usar o H2 em memória para desenvolvimento e teste, mas há também configurações comentadas para usar MySQL com Docker.

## Funcionalidades Principais

### Adicionar Música

- **Endpoint**: `POST /api/music/add`
- Permite adicionar uma nova música à biblioteca.

### Excluir Música

- **Endpoint**: `DELETE /api/music/delete/{id}`
- Permite excluir uma música da biblioteca com base no ID fornecido.

### Atualizar Música

- **Endpoint**: `PUT /api/music/update/{id}`
- Permite atualizar os detalhes de uma música existente com base no ID fornecido.

![Imagem de Exemplo](Assets/img.png)


### Pesquisar Música

- **Endpoint**: `GET /api/music/search`
- Permite pesquisar músicas com base em diferentes critérios, como título, artista ou gênero. Se nenhum parâmetro de pesquisa for fornecido, todas as músicas serão retornadas.

## Documentação da API

A documentação da API pode ser acessada em `http://localhost:8080/swagger-ui.html` quando a aplicação estiver em execução. Esta documentação é gerada automaticamente com base nos endpoints definidos nos controladores REST.

## Execução do Projeto

Para executar o projeto localmente, você pode usar o Maven. Na raiz do projeto, execute o seguinte comando:

```bash
mvn spring-boot:run
```

# Iniciando a Aplicação

Para iniciar a aplicação, certifique-se de que as configurações do banco de dados no arquivo `application.properties` estejam configuradas de acordo com o ambiente em que você está executando a aplicação.

### Ambiente de Desenvolvimento

Durante o desenvolvimento, é possível utilizar o console H2 para visualizar e gerenciar o banco de dados. Ele estará disponível em [http://localhost:8080/h2-console](http://localhost:8080/h2-console) quando a aplicação estiver em execução, utilizando as credenciais configuradas no arquivo `application.properties`.

### Ambiente de Produção

Para ambientes de produção, é recomendável utilizar um banco de dados mais robusto, como o MySQL. Você pode configurar as propriedades do banco de dados no arquivo `application.properties` de acordo com as configurações do seu ambiente de produção.

### Sobre o Projeto

Este é um projeto simples de API de biblioteca de músicas, demonstrando os princípios básicos de desenvolvimento de APIs RESTful com Spring Boot e Spring Data JPA. Sinta-se à vontade para expandir e personalizar conforme necessário para atender aos requisitos do seu projeto.
