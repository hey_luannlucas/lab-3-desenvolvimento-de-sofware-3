package com.api.jala.university.musiclibrary.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusiclibraryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusiclibraryApiApplication.class, args);
	}

}
