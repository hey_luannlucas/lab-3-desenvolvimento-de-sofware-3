package com.api.jala.university.musiclibrary.demo.controller;

import com.api.jala.university.musiclibrary.demo.entity.Music;
import com.api.jala.university.musiclibrary.demo.service.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/music")
public class MusicController {
    @Autowired
    private MusicService musicService;

    @PostMapping("/add")
    public ResponseEntity<Music> addMusic(@RequestBody Music music) {
        Music addedMusic = musicService.addMusic(music);
        return ResponseEntity.ok(addedMusic);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteMusic(@PathVariable Long id) {
        musicService.deleteMusic(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Music> updateMusic(@PathVariable Long id, @RequestBody Music updatedMusic) {
        Music updated = musicService.updateMusic(id, updatedMusic);
        return ResponseEntity.ok(updated);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Music>> search(@RequestParam(required = false) String title,
                                              @RequestParam(required = false) String artist,
                                              @RequestParam(required = false) String genre) {
        List<Music> result;

        // Verifica se nenhum dos parâmetros foi fornecido
        if (title == null && artist == null && genre == null) {
            // Retorna todas as músicas do banco de dados
            result = musicService.getAllMusic();
            return ResponseEntity.ok(result);
        }

        // Caso contrário, realiza a pesquisa com base nos parâmetros fornecidos
        if (title != null) {
            result = musicService.searchByTitle(title);
        } else if (artist != null) {
            result = musicService.searchByArtist(artist);
        } else {
            result = musicService.searchByGenre(genre);
        }

        return ResponseEntity.ok(result);
    }

}

