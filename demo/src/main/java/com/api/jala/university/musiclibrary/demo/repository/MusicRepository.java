package com.api.jala.university.musiclibrary.demo.repository;

import com.api.jala.university.musiclibrary.demo.entity.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MusicRepository extends JpaRepository<Music, Long> {
    List<Music> findByTitleContaining(String title);
    List<Music> findByArtistContaining(String artist);
    List<Music> findByGenre(String genre);
}
