package com.api.jala.university.musiclibrary.demo.service;

import com.api.jala.university.musiclibrary.demo.entity.Music;
import com.api.jala.university.musiclibrary.demo.repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class MusicService {
    @Autowired
    private MusicRepository musicRepository;

    public Music addMusic(Music music) {
        return musicRepository.save(music);
    }

    public void deleteMusic(Long id) {
        musicRepository.deleteById(id);
    }

    public Music updateMusic(Long id, Music updatedMusic) {
        Music existingMusic = musicRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Music not found with id: " + id));

        existingMusic.setTitle(updatedMusic.getTitle());
        existingMusic.setArtist(updatedMusic.getArtist());
        existingMusic.setGenre(updatedMusic.getGenre());
        existingMusic.setDuration(updatedMusic.getDuration());

        return musicRepository.save(existingMusic);
    }
    public List<Music> getAllMusic() {
        return musicRepository.findAll();
    }
    public List<Music> searchByTitle(String title) {
        return musicRepository.findByTitleContaining(title);
    }

    public List<Music> searchByArtist(String artist) {
        return musicRepository.findByArtistContaining(artist);
    }

    public List<Music> searchByGenre(String genre) {
        return musicRepository.findByGenre(genre);
    }

}

